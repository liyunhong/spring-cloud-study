/*
 * 文 件 名： AccessFilter.java 说 明： <文件说明> 版 权： CopyRight (c) 2012 - 2018 所 有 者： 北京创维海通数字技术有限公司 创 建 人：
 * liyunhong 创建时间： 2018年03月09日 变更历史：
 */

package com.lyh.cloud;


import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;


/**
 * 类名： AccessFilter 类说明： <类功能说明> CopyRight： CopyRight (c) 2012 - 2018 Company： HT
 *
 * @author liyunhong
 * @version 1.0
 * @date 2018-03-09
 */
public class AccessFilter extends ZuulFilter
{
    private static Logger log = LoggerFactory.getLogger(AccessFilter.class);

    @Override
    public String filterType()
    {
        return "pre";
    }

    @Override
    public int filterOrder()
    {
        return 0;
    }

    @Override
    public boolean shouldFilter()
    {
        return true;
    }

    @Override
    public Object run()
    {
        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletRequest request = ctx.getRequest();

        log.info("send {} request to {}", request.getMethod(), request.getRequestURL().toString());

        String sign = request.getParameter("sign");
        if (StringUtils.isBlank(sign))
        {
            log.warn("sign is empty");
            ctx.setSendZuulResponse(false);
            ctx.setResponseStatusCode(401);
            return null;
        }

        log.info("access token ok");
        return null;
    }
}
