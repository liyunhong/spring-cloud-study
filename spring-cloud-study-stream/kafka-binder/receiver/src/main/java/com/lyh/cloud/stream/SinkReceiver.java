/*
 * 文 件 名：    SinkReceiver.java 
 * 说    明：   <文件说明>
 * 版    权：   CopyRight (c) 2012 - 2018
 * 所 有 者：   北京创维海通数字技术有限公司
 * 创 建 人：   liyunhong
 * 创建时间：   2018年03月12日
 * 变更历史： 
 *     
 * 
 */

package com.lyh.cloud.stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;

/**
 * 类名： SinkReceiver
 * 类说明： <类功能说明>
 * CopyRight： CopyRight (c) 2012 - 2018
 * Company： HT
 *
 * @author liyunhong
 * @version 1.0
 * @date 2018-03-12
 */
@EnableBinding(Sink.class)
public class SinkReceiver
{
    private static Logger logger = LoggerFactory.getLogger(SinkReceiver.class);

    @StreamListener(Sink.INPUT)
    public void receive(MessageEntity entity)
    {
        logger.info("Received: {} - {}", entity.getId(), entity.getMessage());
    }
}
