/*
 * 文 件 名：    SendMessageController.java 
 * 说    明：   <文件说明>
 * 版    权：   CopyRight (c) 2012 - 2018
 * 所 有 者：   北京创维海通数字技术有限公司
 * 创 建 人：   liyunhong
 * 创建时间：   2018年03月12日
 * 变更历史： 
 *     
 * 
 */

package com.lyh.cloud.controller;

import com.lyh.cloud.stream.SourceMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * 类名： SendMessageController
 * 类说明： <类功能说明>
 * CopyRight： CopyRight (c) 2012 - 2018
 * Company： HT
 *
 * @author liyunhong
 * @version 1.0
 * @date 2018-03-12
 */

@RestController
@RequestMapping("/message")
public class SendMessageController
{
    @Autowired
    private SourceMessageService messageService;

    @GetMapping("send")
    public ResponseEntity send(String message)
    {
        if(StringUtils.isEmpty(message))
        {
            return ResponseEntity.badRequest().body(null);
        }

        Map<String, Object> msgEntity = new HashMap<>();
        msgEntity.put("message", message);
        msgEntity.put("id", UUID.randomUUID());
        messageService.sendMessage(msgEntity);
        return ResponseEntity.ok().body(null);
    }
}
