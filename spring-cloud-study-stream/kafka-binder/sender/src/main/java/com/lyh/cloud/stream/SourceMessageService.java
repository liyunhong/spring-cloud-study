/*
 * 文 件 名： SourceMessageService.java 说 明： <文件说明> 版 权： CopyRight (c) 2012 - 2018 所 有 者：
 * 北京创维海通数字技术有限公司 创 建 人： liyunhong 创建时间： 2018年03月12日 变更历史：
 */

package com.lyh.cloud.stream;


/**
 * 类名： SourceMessageService 类说明： <类功能说明> CopyRight： CopyRight (c) 2012 - 2018 Company： HT
 *
 * @author liyunhong
 * @version 1.0
 * @date 2018-03-12
 */

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.messaging.support.MessageBuilder;


@EnableBinding(Source.class)
public class SourceMessageService
{
    @Autowired
    private Source source;

    public void sendMessage(Object value)
    {
        try
        {
            source.output().send(MessageBuilder.withPayload(value).build());
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
