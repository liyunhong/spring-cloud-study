/*
 * 文 件 名：    UserServiceByDaoImpl.java 
 * 说    明：   <文件说明>
 * 版    权：   CopyRight (c) 2012 - 2018
 * 所 有 者：   北京创维海通数字技术有限公司
 * 创 建 人：   liyunhong
 * 创建时间：   2018年03月12日
 * 变更历史： 
 *     
 * 
 */

package com.lyh.cloud.user.service.impl;

import com.lyh.cloud.user.dao.UserDao;
import com.lyh.cloud.user.entity.User;
import com.lyh.cloud.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 类名： UserServiceByDaoImpl
 * 类说明： <类功能说明>
 * CopyRight： CopyRight (c) 2012 - 2018
 * Company： HT
 *
 * @author liyunhong
 * @version 1.0
 * @date 2018-03-12
 */
@Service
@Qualifier("main")
@Transactional(readOnly = true, rollbackFor = {Exception.class})
public class UserServiceByDaoImpl implements UserService
{
    @Autowired
    private UserDao dao;

    @Override
    public User findOne(Integer id)
    {
        return dao.getById(id);
    }

    @Override
    @Transactional(readOnly = false, rollbackFor = {Exception.class})
    public User save(User user)
    {
        if(user.getId() == null)
        {
            dao.insert(user);
        }
        else
        {
            dao.update(user);
        }

        return user;
    }

    @Override
    @Transactional(readOnly = false, rollbackFor = {Exception.class})
    public void delete(Integer id)
    {
        dao.delete(id);
    }

    @Override
    @Transactional(readOnly = false, rollbackFor = {Exception.class})
    public void delete(User user)
    {
        dao.delete(user);
    }

    @Override
    public boolean exists(Integer id)
    {
        return dao.getById(id) != null;
    }

    @Override
    public Iterable<User> findAll()
    {
        return dao.findList();
    }
}
