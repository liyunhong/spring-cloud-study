/*
 * 文 件 名：    UserServiceImpl.java 
 * 说    明：   <文件说明>
 * 版    权：   CopyRight (c) 2012 - 2018
 * 所 有 者：   北京创维海通数字技术有限公司
 * 创 建 人：   liyunhong
 * 创建时间：   2018年03月08日
 * 变更历史： 
 *     
 * 
 */

package com.lyh.cloud.user.service.impl;

import com.lyh.cloud.user.entity.User;
import com.lyh.cloud.user.repository.UserRepository;
import com.lyh.cloud.user.service.UserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * 类名： UserServiceImpl
 * 类说明： <类功能说明>
 * CopyRight： CopyRight (c) 2012 - 2018
 * Company： HT
 *
 * @author liyunhong
 * @version 1.0
 * @date 2018-03-08
 */
@Service
@Transactional(readOnly = true, rollbackFor = {Exception.class})
public class UserServiceImpl implements UserService
{
    @Resource
    private UserRepository repository;

    @Override
    public User findOne(Integer id)
    {
        return repository.findOne(id);
    }

    @Override
    @Transactional(readOnly = false, rollbackFor = {Exception.class})
    public User save(User user)
    {
        return repository.save(user);
    }

    @Override
    @Transactional(readOnly = false, rollbackFor = {Exception.class})
    public void delete(Integer id)
    {
        repository.delete(id);
    }

    @Override
    @Transactional(readOnly = false, rollbackFor = {Exception.class})
    public void delete(User user)
    {
        repository.delete(user);
    }

    @Override
    public boolean exists(Integer id)
    {
        return repository.exists(id);
    }

    @Override
    public Iterable<User> findAll()
    {
        return repository.findAll();
    }
}
