/*
 * 文 件 名：    UserService.java 
 * 说    明：   <文件说明>
 * 版    权：   CopyRight (c) 2012 - 2018
 * 所 有 者：   北京创维海通数字技术有限公司
 * 创 建 人：   liyunhong
 * 创建时间：   2018年03月08日
 * 变更历史： 
 *     
 * 
 */

package com.lyh.cloud.user.service;

import com.lyh.cloud.user.entity.User;

/**
 * 类名： UserService
 * 类说明： <类功能说明>
 * CopyRight： CopyRight (c) 2012 - 2018
 * Company： HT
 *
 * @author liyunhong
 * @version 1.0
 * @date 2018-03-08
 */
public interface UserService
{
    User findOne(Integer id);

    User save(User user);

    void delete(Integer id);

    void delete(User user);

    boolean exists(Integer id);

    Iterable<User> findAll();
}
