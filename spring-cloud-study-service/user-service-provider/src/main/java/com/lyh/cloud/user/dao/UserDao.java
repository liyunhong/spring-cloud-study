/*
 * 文 件 名：    UserDao.java 
 * 说    明：   <文件说明>
 * 版    权：   CopyRight (c) 2012 - 2018
 * 所 有 者：   北京创维海通数字技术有限公司
 * 创 建 人：   liyunhong
 * 创建时间：   2018年03月12日
 * 变更历史： 
 *     
 * 
 */

package com.lyh.cloud.user.dao;

import com.lyh.cloud.user.common.MyBatisDao;
import com.lyh.cloud.user.entity.User;

import java.util.List;

/**
 * 类名： UserDao
 * 类说明： <类功能说明>
 * CopyRight： CopyRight (c) 2012 - 2018
 * Company： HT
 *
 * @author liyunhong
 * @version 1.0
 * @date 2018-03-12
 */
@MyBatisDao
public interface UserDao
{
    User getById(Integer id);

    int insert(User user);

    int update(User user);

    int delete(Integer id);

    int delete(User user);

    List<User> findList();
}
