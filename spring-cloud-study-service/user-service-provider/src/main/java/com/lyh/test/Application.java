package com.lyh.test;

import com.lyh.cloud.user.common.MyBatisDao;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * Created by Administrator on 2018/7/15.
 */

@SpringBootApplication
@MapperScan(basePackages = "com.lyh.test", annotationClass = MyBatisDao.class)
public class Application {
}
