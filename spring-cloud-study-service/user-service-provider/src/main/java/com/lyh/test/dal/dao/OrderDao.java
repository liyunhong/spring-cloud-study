package com.lyh.test.dal.dao;

import com.lyh.cloud.user.common.MyBatisDao;
import java.util.List;
import java.util.Collection;
import org.apache.ibatis.annotations.Param;
import com.lyh.test.dal.entity.OrderDO;

/**
* 订单 数据访问对象
*/
@MyBatisDao
public interface OrderDao {

    /**
    * 通过主键ID获取订单记录
    * @param id 主键ID
    * @return   如果存在相应记录返回对应的订单记录，否则返回null
    */
    OrderDO getById(Long id);


    /**
    * 通过例子对象获取订单记录
    * @param example 例子对象
    * @return   如果存在相应记录返回对应的订单记录，否则返回null
    *           如果存在多条记录，返回更新时间最晚的记录
    */
    OrderDO queryOne(OrderDO example);

    /**
    * 通过主键ID获取订单记录
    * @param ids 主键ID集合
    * @return   返回满足条件的记录集合
    */
    List<OrderDO> queryByIds(@Param("ids") Collection<Long> ids);


    /**
    * 通过例子对象获取满足条件的订单记录数
    * @param example 例子对象
    * @return   满足条件的记录数
    */
    Long queryCount(OrderDO example);

    /**
    * 通过例子对象获取订单记录,如果start或count为null则不进行分页查询
    * @param example 例子对象
    * @param start   Limit start
    * @param count   Limit count
    * @param orderBy 排序条件
    * @return 返回满足条件的记录集合
    */
    List<OrderDO> queryList(@Param("example") OrderDO example, @Param("start")Integer start, @Param("count") Integer count, @Param("orderBy") String orderBy);

    /**
    * 插入新的订单记录
    * @param entity 订单记录
    * @return 成功返回1，失败返回0
    */
    int insert(OrderDO entity);


    /**
    * 插入新的订单单记录，如果属性为null则该属性不保存
    * @param entity 订单记录
    * @return 成功返回1，失败返回0
    */
    int insertSelective(OrderDO entity);


    /**
    * 插入新的订单记录
    * @param list 订单记录集合
    * @return 成功返回插入的记录数，失败返回0
    */
    int insertBatch(@Param("list") Collection<OrderDO> list);


    /**
    * 更新订单记录，主键不能为空
    * @param entity 新的记录
    * @return
    */
    int update(OrderDO entity);

    /**
    * 更新订单记录，主键不能为空,只更新有值的属性
    * @param entity 新的记录
    * @return
    */
    int updateSelective(OrderDO entity);

    /**
    * 根据主键删除订单记录
    * @param id 主键ID
    * @return 返回删除的记录数
    */
    int deleteById(Long id);


    /**
    * 根据主键删除订单记录
    * @param ids 主键ID集合
    * @return 返回删除的记录数
    */
    int deleteByIds(@Param("ids") Collection<Long> ids);
}