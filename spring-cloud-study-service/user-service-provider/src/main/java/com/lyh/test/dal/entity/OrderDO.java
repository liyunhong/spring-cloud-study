
package com.lyh.test.dal.entity;

import com.lyh.test.dal.entity.BaseEntity;

import java.util.Date;

/**
* 订单 数据对象
*/
public class OrderDO extends BaseEntity{


    /**
    * 收货地址
    */
    private String receiveAddress;

    /**
    * 联系电话
    */
    private String telNumber;

    /**
    * 收货时间
    */
    private Date receiveTime;

    /**
    * 头表编码
    */
    private String headerCode;



    public String getReceiveAddress(){
        return this.receiveAddress;
    }

    public void setReceiveAddress(String receiveAddress){
        this.receiveAddress = receiveAddress;
    }

    public String getTelNumber(){
        return this.telNumber;
    }

    public void setTelNumber(String telNumber){
        this.telNumber = telNumber;
    }

    public Date getReceiveTime(){
        return this.receiveTime;
    }

    public void setReceiveTime(Date receiveTime){
        this.receiveTime = receiveTime;
    }

    public String getHeaderCode(){
        return this.headerCode;
    }

    public void setHeaderCode(String headerCode){
        this.headerCode = headerCode;
    }
}