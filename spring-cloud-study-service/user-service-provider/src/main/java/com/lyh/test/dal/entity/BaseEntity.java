package com.lyh.test.dal.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Administrator on 2018/7/15.
 */
public abstract class BaseEntity implements Serializable{
    private Long id;
    private Date createTime;
    private String createBy;
    private Date updateTime;
    private String updateBy;

    private String orginizationCode;
    private String orginizationName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public String getOrginizationCode() {
        return orginizationCode;
    }

    public void setOrginizationCode(String orginizationCode) {
        this.orginizationCode = orginizationCode;
    }

    public String getOrginizationName() {
        return orginizationName;
    }

    public void setOrginizationName(String orginizationName) {
        this.orginizationName = orginizationName;
    }
}
