package com.lyh.test.dal.dao;

import com.google.common.collect.Lists;
import com.lyh.test.Application;
import com.lyh.test.dal.entity.OrderDO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.Date;
import java.util.List;

@RunWith(SpringRunner.class)
@WebAppConfiguration
@SpringBootTest(classes = {Application.class})
@ActiveProfiles("test")
public class OrderDaoTest {
    @Autowired
    private OrderDao dao;

    private OrderDO create() {
        OrderDO orderDO = new OrderDO();
        Date date = new Date();
        orderDO.setCreateBy("liyunhong");
        orderDO.setUpdateBy("liyunhong");
        orderDO.setCreateTime(date);
        orderDO.setUpdateTime(date);

        return orderDO;
    }

    @Test
    public  void testInsert() {
        List<OrderDO> list = Lists.newArrayList();
        OrderDO orderDO = create();
        orderDO.setReceiveAddress("aaaaa");
        orderDO.setTelNumber("13330330064");
        orderDO.setReceiveTime(new Date());
        list.add(orderDO);

        dao.insert(orderDO);

        orderDO = create();
        orderDO.setReceiveAddress("aaaaa");
        orderDO.setReceiveTime(new Date());

        dao.insert(orderDO);
        list.add(orderDO);

        dao.insertBatch(list);
    }

    @Test
    @Rollback
    public void test() {
        OrderDO orderDO = dao.getById(1L);
        orderDO = new OrderDO();
        orderDO.setId(100L);
        orderDO.setReceiveAddress("aaaa");
        orderDO.setTelNumber("13330330064");

        dao.queryCount(orderDO);
        dao.queryList(orderDO, 0, 10, " a.tel_number ASC");
        dao.queryList(orderDO, null, null, null);
        dao.queryOne(orderDO);

        List<Long> list = Lists.newArrayList();
        list.add(11L);
        list.add(12L);
        list.add(13L);
        List<OrderDO> orderDOList = dao.queryByIds(list);
        orderDOList.stream().forEach(x -> System.out.println(x.getTelNumber()));

        dao.deleteById(11L);
        dao.deleteByIds(list);
    }


    @Test
    public void testLeftJoin()
    {
        OrderDO orderDO = dao.getById(1L);
        System.out.println(orderDO.getOrginizationCode() + "=>" + orderDO.getOrginizationName());
        orderDO = dao.queryOne(orderDO);
        System.out.println(orderDO.getOrginizationCode() + "=>" + orderDO.getOrginizationName());


        List<Long> ids = Lists.asList(1L , 2L, new Long[0]);
        List<OrderDO> list = dao.queryByIds(ids);

        for(OrderDO o : list){
            System.out.println(orderDO.getOrginizationCode() + "=>" + orderDO.getOrginizationName());
        }

        OrderDO query = new  OrderDO();
        query.setHeaderCode("123456");
        list = dao.queryList(query, null, null, null);
        for(OrderDO o : list){
            System.out.println(orderDO.getOrginizationCode() + "=>" + orderDO.getOrginizationName());
        }
    }

    @Test
    public void testUpdate(){
        OrderDO orderDO = new OrderDO();
        orderDO.setId(21L);
        orderDO.setReceiveAddress("BBBBBBBBBBB");
        orderDO.setReceiveAddress("XXXXXXXXXXXXXXXXXXXXXXX");
        Date date = new Date();
        orderDO.setCreateBy("liyunhong2");
        orderDO.setUpdateBy("liyunhong2");
        orderDO.setCreateTime(date);
        orderDO.setUpdateTime(date);

        dao.updateSelective(orderDO);

        orderDO.setTelNumber("23456789");
        orderDO.setReceiveTime(date);
        dao.update(orderDO);
    }
}