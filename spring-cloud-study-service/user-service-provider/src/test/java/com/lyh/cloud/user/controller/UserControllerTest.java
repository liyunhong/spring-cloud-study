package com.lyh.cloud.user.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lyh.cloud.user.Application;
import com.lyh.cloud.user.entity.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebAppConfiguration
@SpringBootTest(classes = {Application.class})
@ActiveProfiles("test")
public class UserControllerTest
{
    @Autowired
    private UserController userController;

    private MockMvc mvc;

    @Before
    public void setUp()
    {
        mvc = MockMvcBuilders.standaloneSetup(userController).build();
    }

    @Test
    public void findAllTest() throws Exception
    {
        ResultActions resultActions = mvc.perform(get("/users").accept(MediaType.APPLICATION_JSON));
        resultActions.andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(jsonPath("$.[0].id").value(1));
    }

    @Test
    public void getByIdTest() throws Exception
    {
        Integer id = 4;
        ResultActions resultActions = mvc.perform(get("/user/" + id).accept(MediaType.APPLICATION_JSON));
        resultActions.andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(jsonPath("$.id").value(id));
    }

    @Test
    @Rollback
    @Transactional
    public void addTest() throws Exception
    {
        User u = new User();
        u.setName("123456");
        u.setAge(30);
        String jsonBody = new ObjectMapper().writeValueAsString(u);
        ResultActions resultActions = mvc.perform(post("/user")
            .contentType(MediaType.APPLICATION_JSON_UTF8)
            .content(jsonBody)
            .accept(MediaType.APPLICATION_JSON));
        resultActions.andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(jsonPath("$.age").value(u.getAge()));
    }

    @Test
    @Rollback
    @Transactional
    public void updateTest() throws Exception
    {
        User u = new User();
        u.setName("123456");
        u.setAge(100);
        Integer userId = 4;

        String jsonBody = new ObjectMapper().writeValueAsString(u);
        ResultActions resultActions = mvc.perform(put("/user/" + userId)
            .contentType(MediaType.APPLICATION_JSON_UTF8)
            .content(jsonBody)
            .accept(MediaType.APPLICATION_JSON));
        resultActions.andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(jsonPath("$.age").value(u.getAge()));
    }


    @Test
    @Rollback
    @Transactional
    public void deleteTest() throws Exception
    {
        Integer userId = 4;
        ResultActions resultActions = mvc.perform(delete("/user/" + userId)
            .contentType(MediaType.APPLICATION_JSON_UTF8)
            .accept(MediaType.APPLICATION_JSON));

        resultActions.andExpect(status().isOk());
    }
}