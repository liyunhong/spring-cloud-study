/*
 * 文 件 名：    CustomRibbonConfiguration.java 
 * 说    明：   <文件说明>
 * 版    权：   CopyRight (c) 2012 - 2018
 * 所 有 者：   北京创维海通数字技术有限公司
 * 创 建 人：   liyunhong
 * 创建时间：   2018年03月13日
 * 变更历史： 
 *     
 * 
 */

package com.lyh.cloud.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.netflix.client.config.IClientConfig;
import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;

/**
 * 类名： CustomRibbonConfiguration
 * 类说明： <类功能说明>
 * CopyRight： CopyRight (c) 2012 - 2018
 * Company： HT
 *
 * @author liyunhong
 * @version 1.0
 * @date 2018-03-13
 */

@Configuration
public class CustomRibbonConfiguration
{
    @Autowired
    private IClientConfig config;

    @Bean
    public IRule ribbonRule(IClientConfig config)
    {
        RandomRule rule = new RandomRule();
        rule.initWithNiwsConfig(config);
        return rule;
    }
}
