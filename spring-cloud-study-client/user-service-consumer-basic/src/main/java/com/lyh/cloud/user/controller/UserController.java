/*
 * 文 件 名：    UserController.java 
 * 说    明：   <文件说明>
 * 版    权：   CopyRight (c) 2012 - 2018
 * 所 有 者：   北京创维海通数字技术有限公司
 * 创 建 人：   liyunhong
 * 创建时间：   2018年03月08日
 * 变更历史： 
 *     
 * 
 */

package com.lyh.cloud.user.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.lyh.cloud.user.entity.User;

/**
 * 类名： UserController
 * 类说明： <类功能说明>
 * CopyRight： CopyRight (c) 2012 - 2018
 * Company： HT
 *
 * @author liyunhong
 * @version 1.0
 * @date 2018-03-08
 */

@RestController
public class UserController
{
    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    @Autowired
    LoadBalancerClient loadBalancerClient;

    @Value("${services.userService.name}")
    private String userServiceName;


    @Autowired
    RestTemplate restTemplate;

    @GetMapping("user/{userId}")
    public ResponseEntity getById(@PathVariable(name = "userId", required = true) Integer id)
    {
        ServiceInstance serviceInstance = loadBalancerClient.choose(userServiceName);
        String url = "http://" + serviceInstance.getHost() + ":" + serviceInstance.getPort() + "/user/" +  id.toString();
        LOGGER.info("request url : {}", url);
        return restTemplate.getForEntity(url, User.class);
    }
}
