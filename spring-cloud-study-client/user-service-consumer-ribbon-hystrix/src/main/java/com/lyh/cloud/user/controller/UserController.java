/*
 * 文 件 名：    UserController.java 
 * 说    明：   <文件说明>
 * 版    权：   CopyRight (c) 2012 - 2018
 * 所 有 者：   北京创维海通数字技术有限公司
 * 创 建 人：   liyunhong
 * 创建时间：   2018年03月08日
 * 变更历史： 
 *     
 * 
 */

package com.lyh.cloud.user.controller;

import com.lyh.cloud.user.entity.User;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;

/**
 * 类名： UserController
 * 类说明： <类功能说明>
 * CopyRight： CopyRight (c) 2012 - 2018
 * Company： HT
 *
 * @author liyunhong
 * @version 1.0
 * @date 2018-03-08
 */

@RestController
public class UserController
{
    private static Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    RestTemplate restTemplate;

    @Value("${services.userService.name}")
    private String userServiceName;

    @GetMapping("user/{userId}")
    @HystrixCommand(fallbackMethod = "getByIdMock")
    public ResponseEntity getById(@PathVariable(name = "userId", required = true) Integer id, HttpServletRequest request)
    {
        logger.info("call getById from {}", request.getRemoteHost());
        return restTemplate.getForEntity("http://"+userServiceName+"/user/" + String.valueOf(id), User.class);
    }


    public ResponseEntity getByIdMock(Integer id, HttpServletRequest request)
    {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
    }

    @GetMapping("users")
    @HystrixCommand(fallbackMethod = "findAllMock")
    public  ResponseEntity findAll(HttpServletRequest request)
    {
        logger.info("call findAll from {}", request.getRemoteHost());
        return restTemplate.getForEntity("http://"+userServiceName+"/users" , Collection.class);
    }

    public ResponseEntity findAllMock(HttpServletRequest request)
    {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
    }
}
