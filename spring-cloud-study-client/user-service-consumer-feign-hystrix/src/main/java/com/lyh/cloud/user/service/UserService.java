/*
 * 文 件 名：    UserService.java 
 * 说    明：   <文件说明>
 * 版    权：   CopyRight (c) 2012 - 2018
 * 所 有 者：   北京创维海通数字技术有限公司
 * 创 建 人：   liyunhong
 * 创建时间：   2018年03月08日
 * 变更历史： 
 *     
 * 
 */

package com.lyh.cloud.user.service;

import com.lyh.cloud.user.entity.User;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

/**
 * 类名： UserService
 * 类说明： <类功能说明>
 * CopyRight： CopyRight (c) 2012 - 2018
 * Company： HT
 *
 * @author liyunhong
 * @version 1.0
 * @date 2018-03-08
 */
@FeignClient(value = "${services.userService.name}", fallback = UserServiceFallback.class)
public interface UserService
{
    @GetMapping("/user/{userId}")
    User getById(@PathVariable(name = "userId", required = true) Integer id);

    @GetMapping("/users")
    List<User> findAll();


    @PostMapping(value = "/user", consumes = MediaType.APPLICATION_JSON_VALUE)
    User add(User user);

    @DeleteMapping(value = "/user/{userId}")
    ResponseEntity deleteById(@PathVariable(name = "userId", required = true) Integer id);
}
