/*
 * 文 件 名：    UserServiceFallback.java 
 * 说    明：   <文件说明>
 * 版    权：   CopyRight (c) 2012 - 2018
 * 所 有 者：   北京创维海通数字技术有限公司
 * 创 建 人：   liyunhong
 * 创建时间：   2018年03月08日
 * 变更历史： 
 *     
 * 
 */

package com.lyh.cloud.user.service;

import com.lyh.cloud.user.entity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

/**
 * 类名： UserServiceFallback
 * 类说明： <类功能说明>
 * CopyRight： CopyRight (c) 2012 - 2018
 * Company： HT
 *
 * @author liyunhong
 * @version 1.0
 * @date 2018-03-08
 */
@Component
public class UserServiceFallback implements UserService
{
    private static Logger logger = LoggerFactory.getLogger(UserServiceFallback.class);

    @Override
    public User getById(@PathVariable(name = "userId", required = true) Integer id)
    {
        logger.info("current thread is {}-{}", Thread.currentThread().getId(), Thread.currentThread().getName());
        return null;
    }

    @Override
    public List<User> findAll()
    {
        return null;
    }

    @Override
    public User add(User user)
    {
        return null;
    }

    @Override
    public ResponseEntity deleteById(@PathVariable(name = "userId", required = true) Integer id)
    {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
    }
}
