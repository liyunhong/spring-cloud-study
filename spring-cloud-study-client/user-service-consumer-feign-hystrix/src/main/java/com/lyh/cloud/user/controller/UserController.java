/*
 * 文 件 名：    UserController.java 
 * 说    明：   <文件说明>
 * 版    权：   CopyRight (c) 2012 - 2018
 * 所 有 者：   北京创维海通数字技术有限公司
 * 创 建 人：   liyunhong
 * 创建时间：   2018年03月08日
 * 变更历史： 
 *     
 * 
 */

package com.lyh.cloud.user.controller;

import com.lyh.cloud.user.entity.User;
import com.lyh.cloud.user.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.UUID;

/**
 * 类名： UserController
 * 类说明： <类功能说明>
 * CopyRight： CopyRight (c) 2012 - 2018
 * Company： HT
 *
 * @author liyunhong
 * @version 1.0
 * @date 2018-03-08
 */

@RestController
public class UserController
{
    private static Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    UserService userService;

    @GetMapping("user/{userId}")
    public ResponseEntity getById(@PathVariable(name = "userId", required = true) Integer id, HttpServletRequest request)
    {
        logger.info("current thread is {}-{}", Thread.currentThread().getId(), Thread.currentThread().getName());
        logger.info("call getById from {}", request.getRemoteHost());
        User user = userService.getById(id);
        if(user == null)
        {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok().body(user);
    }

    @GetMapping(value = "remove/{userId}")
    public ResponseEntity delete(@PathVariable(name = "userId", required = true) Integer id)
    {
        return userService.deleteById(id);
    }

    @GetMapping("users")
    public ResponseEntity findAll(User user, HttpServletRequest request)
    {
        logger.info("call findAll from {}", request.getRemoteHost());
        Iterable<User> all = userService.findAll();
        if(all == null)
        {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(all);
    }


    @GetMapping("gen")
    public ResponseEntity add()
    {
        User user = new User();
        Double randomAge = Math.random() * 100;
        user.setAge(randomAge.intValue());
        user.setName(UUID.randomUUID().toString().replaceAll("-", ""));
        user = userService.add(user);

        if(user == null)
        {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok().body(user);
    }

}
