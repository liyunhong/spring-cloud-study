/*
 * 文 件 名： Application.java 说 明： <文件说明> 版 权： CopyRight (c) 2012 - 2018 所 有 者： 北京创维海通数字技术有限公司 创 建 人：
 * liyunhong 创建时间： 2018年03月08日 变更历史：
 */

package com.lyh.config;


import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.config.server.EnableConfigServer;


/**
 * 类名： Application 类说明： <类功能说明> CopyRight： CopyRight (c) 2012 - 2018 Company： HT
 *
 * @author liyunhong
 * @version 1.0
 * @date 2018-03-08
 */
@EnableConfigServer
@SpringBootApplication
public class Application
{

    public static void main(String[] args)
    {
        new SpringApplicationBuilder(Application.class).web(true).run(args);
    }

}
