java -Xmx128m -Xms128m -jar D:\DevelopSpace4\spring-cloud-study\spring-cloud-study-config-server\config-server\target\config-server-1.0-SNAPSHOT.jar

java -Xmx128m -Xms128m -jar D:\DevelopSpace4\spring-cloud-study\spring-cloud-study-eureka-server\eureka-server\target\eureka-server-1.0-SNAPSHOT.jar --spring.profiles.active=node01
java -Xmx128m -Xms128m -jar D:\DevelopSpace4\spring-cloud-study\spring-cloud-study-eureka-server\eureka-server\target\eureka-server-1.0-SNAPSHOT.jar --spring.profiles.active=node02

java -Xmx128m -Xms128m -jar D:\DevelopSpace4\spring-cloud-study\spring-cloud-study-service\user-service-provider\target\user-service-provider-1.0-SNAPSHOT.jar --spring.profiles.active=node01
java -Xmx128m -Xms128m -jar D:\DevelopSpace4\spring-cloud-study\spring-cloud-study-service\user-service-provider\target\user-service-provider-1.0-SNAPSHOT.jar --spring.profiles.active=node02